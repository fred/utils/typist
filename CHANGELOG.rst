ChangeLog
=========

Unreleased
----------


2.0.0 (2024-09-24)
------------------

* **[BREAKING]** Upgrade to pydantic 2 (#16)

1.1.2 (2024-09-24)
------------------

* Rename to ``fred-typist`` (#12)

1.1.1 (2024-09-23)
------------------

* Log client errors (#14)

1.1.0 (2024-03-25)
------------------

* Drop support for python 3.7
* Add support for python 3.12
* Add support for pydantic 2 (#13)
* Update project setup

1.0.1 (2023-04-13)
------------------

* Fix template name enums in Python 3.11 (#10)
* Reformat with black

1.0.0 (2022-08-22)
------------------

* **[BREAKING]** Change return types of ``get_template_info`` and ``get_template_collections``
  to pydantic models
* Add template collection labels
* Add ``json_encoder`` arg to ``SecretaryClient``, encode some common types from stdlib by default

0.5.1 (2022-07-20)
------------------

* Add timeout attribute to client methods

0.5.0 (2022-05-03)
------------------

* Add support for authentication (#6)
* Update project setup

0.4.0 (2022-03-14)
------------------

* Add render_pdf method
* Convert markdown to rst
* Update supported python versions
  * Drop 3.5 and 3.6
  * Add 3.9 and 3.10


0.3.0 (2020-10-19)
------------------

* Add template collections list method


0.2.0 (2020-08-25)
------------------

* Add license
* Add error message to TemplateNotFound exception


0.1.0 (2020-08-11)
------------------

* Add django-secretary client with render and render_html methods
