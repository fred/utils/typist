"""REST API handler for typist."""

from typing import Any, Sequence

import requests

from typist import client


def get_all_results(url: str, *args: Any, **kwargs: Any) -> Sequence:
    """Get all results from paginated REST API.

    This presumes that server is returning consistent results,
    meaning that all pages combined contain all the items and
    each item is present in just one page.
    """
    response = client.process_errors(requests.get)(url, *args, **kwargs)

    response_json = response.json()
    results: list = response_json["results"]

    while response_json.get("next"):
        response = client.process_errors(requests.get)(response_json["next"], *args, **kwargs)
        response_json = response.json()
        results.extend(response_json["results"])

    return results
