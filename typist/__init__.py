"""Typist - client lib for django-secretary."""

from .client import HTTPTokenAuth, SecretaryClient
from .encoder import SecretaryJsonEncoder
from .schema import Template, TemplateCollection

__version__ = "2.0.0"

__all__ = [
    "HTTPTokenAuth",
    "SecretaryClient",
    "SecretaryJsonEncoder",
    "Template",
    "TemplateCollection",
]
