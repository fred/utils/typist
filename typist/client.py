"""Client for django-secretary."""

import json
import logging
from enum import Enum
from functools import wraps
from typing import Any, Callable, List, Optional, Sequence, Tuple, Type, TypeVar, Union, cast
from urllib.parse import urljoin
from uuid import UUID

import requests
from requests import HTTPError, PreparedRequest, RequestException, Response
from requests.auth import AuthBase

from typist import rest

from .encoder import SecretaryJsonEncoder
from .schema import Template, TemplateCollection

Timeout = Union[float, Tuple[float, float]]

_LOGGER = logging.getLogger(__name__)


class TemplateNotFound(ValueError):
    """Template not found exception."""


_RequestsCallable = TypeVar("_RequestsCallable", bound=Callable[..., Response])


def process_errors(func: _RequestsCallable) -> _RequestsCallable:
    """Process commonly raised connection errors.

    Args:
        func: Function returning the requests Response object.

    Raises:
        TemplateNotFound: If response status code is 404.
        ValueError: In case of any other HTTP error (such as 403 or 500).
        RuntimeError: In case of any other error (such as ConnectionError).
    """

    @wraps(func)
    def wrapper(url: str, *args: Any, **kwargs: Any) -> Response:
        try:
            response = func(url, *args, **kwargs)
            if response.status_code == 404:
                _LOGGER.info("Template not found at url: %s", response.request.url)
                raise TemplateNotFound(
                    "Template not found at url: {}".format(response.request.url)
                )
            response.raise_for_status()
        except HTTPError as error:
            if response.status_code >= 400 and response.status_code < 500:
                _LOGGER.info("Error returned from template [%r]: %s", url, error)
                raise ValueError(str(error)) from error
            else:
                _LOGGER.warning("Error returned from template [%r]: %s", url, error)
                raise RuntimeError(str(error)) from error
        except RequestException as error:
            _LOGGER.warning("Error returned from template [%r]: %s", url, error)
            raise RuntimeError(str(error)) from error

        return response

    return cast(_RequestsCallable, wrapper)


class SecretaryClient:
    """Client for django-secretary."""

    def __init__(
        self,
        server_url: str,
        timeout: Timeout = 3.05,
        *,
        auth: Optional[Union[AuthBase, Tuple[str, str]]] = None,
        json_encoder: Type[json.JSONEncoder] = SecretaryJsonEncoder,
    ):
        """Initialize REST API client.

        Args:
            server_url: Server url of django-secretary app, including the path
                to the REST API with trailing slash.
            timeout: Timeout for the django-secretary API.
            auth: Authentication for the django-secreatry API.
                  See https://docs.python-requests.org/en/latest/user/authentication/
                  for details.
            json_encoder: Class of custom JSON encoder used to encode template context.
        """
        self.server_url = server_url
        self.timeout = timeout
        self.auth = auth
        self.json_encoder = json_encoder

    def render(
        self, template: Union[UUID, str], context: dict, *, timeout: Optional[Timeout] = None
    ) -> str:
        """Render plaintext template.

        Args:
            template: Name of template or UUID.
            context: Context passed to template for rendering.
            timeout: Timeout for the django-secretary API.

        Returns:
            Rendered plain text template.

        Raises:
            TemplateNotFound: If response status code is 404.
            ValueError: In case of any other HTTP error (such as 403 or 500).
            RuntimeError: In case of any other error (such as ConnectionError).
        """
        response = self._get_render_response(
            template, context, accept="text/plain", timeout=timeout
        )
        return response.text

    def render_html(
        self, template: Union[UUID, str], context: dict, *, timeout: Optional[Timeout] = None
    ) -> str:
        """Render HTML template.

        Args:
            template: Name of template or UUID.
            context: Context passed to template for rendering.
            timeout: Timeout for the django-secretary API.

        Returns:
            Rendered HTML template.

        Raises:
            TemplateNotFound: If response status code is 404.
            ValueError: In case of any other HTTP error (such as 403 or 500).
            RuntimeError: In case of any other error (such as ConnectionError).
        """
        response = self._get_render_response(
            template, context, accept="text/html", timeout=timeout
        )
        return response.text

    def render_pdf(
        self, template: Union[UUID, str], context: dict, *, timeout: Optional[Timeout] = None
    ) -> bytes:
        """Render PDF template.

        Args:
            template: Name of template or UUID.
            context: Context passed to template for rendering.
            timeout: Timeout for the django-secretary API.

        Returns:
            Rendered PDF.

        Raises:
            TemplateNotFound: If response status code is 404.
            ValueError: In case of any other HTTP error (such as 403 or 500).
            RuntimeError: In case of any other error (such as ConnectionError).
        """
        response = self._get_render_response(
            template, context, accept="application/pdf", timeout=timeout
        )
        return response.content

    def get_template_info(
        self, template: Union[UUID, str], *, timeout: Optional[Timeout] = None
    ) -> Template:
        """Get template info.

        Args:
            template: Name of template or UUID.
            timeout: Timeout for the django-secretary API.

        Returns:
            Template info (name, uuid, is_active, assets, ...).

        Raises:
            TemplateNotFound: If response status code is 404.
            ValueError: In case of any other HTTP error (such as 403 or 500).
            RuntimeError: In case of any other error (such as ConnectionError).
        """
        response = process_errors(requests.get)(
            self._get_template_url(template),
            timeout=timeout or self.timeout,
            auth=self.auth,
        )

        return Template(**response.json())

    def get_template_collections(
        self, tags: Optional[Sequence[str]] = None, *, timeout: Optional[Timeout] = None
    ) -> List[TemplateCollection]:
        """Get list of template collections.

        Args:
            tags: Return only template collections with at least one of these tags.
            timeout: Timeout for the django-secretary API.

        Returns:
            List of template collections.
        """
        if not tags:
            tags = [""]

        collections = {}
        for tag in tags:
            results = rest.get_all_results(
                urljoin(self.server_url, "template-collections/"),
                params={"tags": tag},
                timeout=timeout or self.timeout,
                auth=self.auth,
            )
            for col in results:
                collections[col["name"]] = TemplateCollection(**col)

        return list(collections.values())

    def _get_template_url(self, template: Union[UUID, str]) -> str:
        """Get template url."""
        if isinstance(template, UUID):
            return urljoin(self.server_url, "templates/{}/".format(template))
        elif isinstance(template, Enum):
            return urljoin(self.server_url, "templates/active/{}/".format(template.value))
        else:
            return urljoin(self.server_url, "templates/active/{}/".format(template))

    def _get_render_response(
        self,
        template: Union[UUID, str],
        context: dict,
        *,
        accept: str,
        timeout: Optional[Timeout] = None,
    ) -> Response:
        """Render template."""
        response = process_errors(requests.post)(  # nosec B113 # (bandit thinks that we don't use timeout here)
            urljoin(self._get_template_url(template), "render/"),
            headers={"Content-Type": "application/json", "Accept": accept},
            data=json.dumps({"context": context}, cls=self.json_encoder),
            timeout=timeout or self.timeout,
            auth=self.auth,
        )

        return response


class HTTPTokenAuth(AuthBase):
    """Simple token authentication."""

    def __init__(self, token: str):
        self.token = token

    def __call__(self, request: PreparedRequest) -> PreparedRequest:
        """Attach the token to the request."""
        request.headers["Authorization"] = "Token " + self.token
        return request
