"""JSON encoder for secretary context."""

from datetime import datetime
from decimal import Decimal
from ipaddress import IPv4Address, IPv6Address
from json import JSONEncoder
from typing import Any
from uuid import UUID


class SecretaryJsonEncoder(JSONEncoder):
    """JSON encoder for secretary context."""

    def default(self, obj: Any) -> Any:
        """Decode custom types."""
        if isinstance(obj, (Decimal, IPv4Address, IPv6Address, UUID)):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()

        return super().default(obj)
