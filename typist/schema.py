"""API schema for typist."""

from datetime import datetime
from typing import Optional, Set

from pydantic import BaseModel


class Template(BaseModel):
    """Template info."""

    name: str
    is_active: bool
    uuid: str
    create_datetime: datetime
    parent: Optional[str] = None
    asset_set: Set[str] = set()


class TemplateCollection(BaseModel):
    """Template collection."""

    name: str
    label: str
    tags: Set[str] = set()
