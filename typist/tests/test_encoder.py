import json
from datetime import datetime, timezone
from decimal import Decimal
from ipaddress import IPv4Address, IPv6Address
from unittest import TestCase
from uuid import UUID

from typist.encoder import SecretaryJsonEncoder


class SecretaryJsonEncoderTest(TestCase):
    def test_encode(self):
        values = (
            (datetime(2022, 1, 1), '"2022-01-01T00:00:00"'),
            (datetime(2022, 1, 1, tzinfo=timezone.utc), '"2022-01-01T00:00:00+00:00"'),
            (Decimal("3.14"), '"3.14"'),
            (IPv4Address("127.0.0.1"), '"127.0.0.1"'),
            (IPv6Address("::1"), '"::1"'),
            (UUID(int=1), '"00000000-0000-0000-0000-000000000001"'),
        )

        for origin, encoded in values:
            with self.subTest(origin=origin, encoded=encoded):
                self.assertEqual(json.dumps(origin, cls=SecretaryJsonEncoder), encoded)

    def test_unknown(self):
        class Unknown:
            """Unknown type."""

        with self.assertRaises(TypeError):
            json.dumps(Unknown(), cls=SecretaryJsonEncoder)
