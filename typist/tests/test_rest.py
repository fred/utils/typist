from unittest import TestCase

import responses

from typist.rest import get_all_results


class GetAllResultsTest(TestCase):
    @responses.activate
    def test_get_all_results(self):
        responses.add(
            responses.GET,
            "http://example.com/api/",
            json={
                "previous": None,
                "next": "http://example.com/api/?page=2",
                "results": [0, 1],
            },
        )
        responses.add(
            responses.GET,
            "http://example.com/api/",
            json={
                "previous": "http://example.com/api/?page=1",
                "next": "http://example.com/api/?page=3",
                "results": [2, 3],
            },
        )
        responses.add(
            responses.GET,
            "http://example.com/api/",
            json={
                "previous": "http://example.com/api/?page=2",
                "next": None,
                "results": [4, 5],
            },
        )
        self.assertEqual(get_all_results("http://example.com/api/"), [0, 1, 2, 3, 4, 5])
