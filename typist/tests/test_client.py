from datetime import datetime, timezone
from enum import Enum
from unittest import TestCase
from unittest.mock import sentinel
from uuid import UUID

import requests
import responses
from requests import PreparedRequest
from responses.matchers import header_matcher, request_kwargs_matcher
from testfixtures import LogCapture

from typist.client import HTTPTokenAuth, SecretaryClient, TemplateNotFound, process_errors
from typist.schema import Template, TemplateCollection


class TestSecretaryClient(TestCase):
    def setUp(self):
        self.client = SecretaryClient("http://example.com/api/", timeout=sentinel.default_timeout)
        self.uuid = UUID(int=42)
        self.name = "template.txt"
        self.template_json = {
            "name": self.name,
            "is_active": True,
            "uuid": str(self.uuid),
            "create_datetime": datetime(2022, 1, 1, tzinfo=timezone.utc).isoformat(),
        }
        self.template = Template(
            name=self.name,
            is_active=True,
            uuid=str(self.uuid),
            create_datetime=datetime(2022, 1, 1, tzinfo=timezone.utc),
        )

    @responses.activate
    def test_render_by_uuid(self):
        url = "http://example.com/api/templates/{}/render/".format(self.uuid)
        responses.add(
            responses.POST,
            url,
            body=b"rendered template",
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.render(self.uuid, context={}, timeout=sentinel.timeout),
            "rendered template",
        )
        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/plain")

    @responses.activate
    def test_render_by_name(self):
        url = "http://example.com/api/templates/active/{}/render/".format(self.name)
        responses.add(
            responses.POST,
            url,
            body=b"rendered template",
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.render(self.name, context={}, timeout=sentinel.timeout),
            "rendered template",
        )
        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/plain")

    @responses.activate
    def test_render_by_enum_name(self):
        class Template(str, Enum):
            INIT = "init"

        url = "http://example.com/api/templates/active/{}/render/".format(Template.INIT.value)
        responses.add(
            responses.POST,
            url,
            body=b"rendered template",
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.render(Template.INIT, context={}, timeout=sentinel.timeout),
            "rendered template",
        )
        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/plain")

    @responses.activate
    def test_render_html_by_uuid(self):
        url = "http://example.com/api/templates/{}/render/".format(self.uuid)
        responses.add(
            responses.POST,
            url,
            body=b"<b>rendered</b> template",
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.render_html(self.uuid, context={}, timeout=sentinel.timeout),
            "<b>rendered</b> template",
        )
        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/html")

    @responses.activate
    def test_render_html_by_name(self):
        url = "http://example.com/api/templates/active/{}/render/".format(self.name)
        responses.add(
            responses.POST,
            url,
            body=b"<b>rendered</b> template",
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.render_html(self.name, context={}, timeout=sentinel.timeout),
            "<b>rendered</b> template",
        )
        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/html")

    @responses.activate
    def test_render_pdf_by_uuid(self):
        url = "http://example.com/api/templates/{}/render/".format(self.uuid)
        match = [
            header_matcher({"Accept": "application/pdf"}),
            request_kwargs_matcher({"timeout": sentinel.timeout}),
        ]
        responses.add(responses.POST, url, body=b"rendered template", match=match)

        self.assertEqual(
            self.client.render_pdf(self.uuid, context={}, timeout=sentinel.timeout),
            b"rendered template",
        )

        responses.assert_call_count(url, 1)

    @responses.activate
    def test_render_pdf_by_name(self):
        url = "http://example.com/api/templates/active/{}/render/".format(self.name)
        match = [
            header_matcher({"Accept": "application/pdf"}),
            request_kwargs_matcher({"timeout": sentinel.timeout}),
        ]
        responses.add(responses.POST, url, body=b"rendered template", match=match)

        self.assertEqual(
            self.client.render_pdf(self.name, context={}, timeout=sentinel.timeout),
            b"rendered template",
        )

        responses.assert_call_count(url, 1)

    @responses.activate
    def test_render_auth(self):
        # Test authentication in render methods.
        url = "http://example.com/api/templates/{}/render/".format(self.uuid)
        responses.add(
            responses.POST,
            url,
            body=b"rendered template",
            match=[header_matcher({"Authorization": "Basic cmltbWVyOmdhenBhY2hvIQ=="})],
        )
        client = SecretaryClient("http://example.com/api/", auth=("rimmer", "gazpacho!"))

        self.assertEqual(client.render(self.uuid, context={}), "rendered template")

        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/plain")

    @responses.activate
    def test_render_default_timeout(self):
        url = "http://example.com/api/templates/{}/render/".format(self.uuid)
        responses.add(
            responses.POST,
            url,
            body=b"rendered template",
            match=[request_kwargs_matcher({"timeout": sentinel.default_timeout})],
        )
        self.assertEqual(self.client.render(self.uuid, context={}), "rendered template")
        responses.assert_call_count(url, 1)
        self.assertEqual(responses.calls[0].request.headers["Accept"], "text/plain")

    @responses.activate
    def test_get_template_info_by_uuid(self):
        url = "http://example.com/api/templates/{}/".format(self.uuid)
        responses.add(
            responses.GET,
            url,
            json=self.template_json,
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.get_template_info(self.uuid, timeout=sentinel.timeout), self.template
        )
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_info_by_name(self):
        url = "http://example.com/api/templates/active/{}/".format(self.name)
        responses.add(
            responses.GET,
            url,
            json=self.template_json,
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.get_template_info(self.name, timeout=sentinel.timeout), self.template
        )
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_info_auth(self):
        # Test authentication in get_template_info method.
        url = "http://example.com/api/templates/{}/".format(self.uuid)
        responses.add(
            responses.GET,
            url,
            json=self.template_json,
            match=[header_matcher({"Authorization": "Basic cmltbWVyOmdhenBhY2hvIQ=="})],
        )

        client = SecretaryClient("http://example.com/api/", auth=("rimmer", "gazpacho!"))

        self.assertEqual(client.get_template_info(self.uuid), self.template)
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_info_default_timeout(self):
        url = "http://example.com/api/templates/{}/".format(self.uuid)
        responses.add(
            responses.GET,
            url,
            json=self.template_json,
            match=[request_kwargs_matcher({"timeout": sentinel.default_timeout})],
        )
        self.assertEqual(self.client.get_template_info(self.uuid), self.template)
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_collections_all(self):
        url = "http://example.com/api/template-collections/?tags="
        responses.add(
            responses.GET,
            url,
            json={
                "results": [
                    {"name": "warning_email", "label": "Warning"},
                    {"name": "error_email", "label": "Error"},
                ]
            },
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.get_template_collections(timeout=sentinel.timeout),
            [
                TemplateCollection(name="warning_email", label="Warning"),
                TemplateCollection(name="error_email", label="Error"),
            ],
        )
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_collections_by_tag(self):
        url = "http://example.com/api/template-collections/?tags=warning"
        responses.add(
            responses.GET,
            url,
            json={
                "results": [
                    {"name": "warning_email", "label": "Warning"},
                ]
            },
            match=[request_kwargs_matcher({"timeout": sentinel.timeout})],
        )
        self.assertEqual(
            self.client.get_template_collections(tags=["warning"], timeout=sentinel.timeout),
            [TemplateCollection(name="warning_email", label="Warning")],
        )
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_collections_auth(self):
        # Test authentication in get_template_collections.
        url = "http://example.com/api/template-collections/?tags="
        responses.add(
            responses.GET,
            url,
            json={
                "results": [
                    {"name": "warning_email", "label": "Warning"},
                    {"name": "error_email", "label": "Error"},
                ]
            },
            match=[header_matcher({"Authorization": "Basic cmltbWVyOmdhenBhY2hvIQ=="})],
        )

        client = SecretaryClient("http://example.com/api/", auth=("rimmer", "gazpacho!"))

        self.assertEqual(
            client.get_template_collections(),
            [
                TemplateCollection(name="warning_email", label="Warning"),
                TemplateCollection(name="error_email", label="Error"),
            ],
        )
        responses.assert_call_count(url, 1)

    @responses.activate
    def test_get_template_collections_all_default_timeout(self):
        url = "http://example.com/api/template-collections/?tags="
        responses.add(
            responses.GET,
            url,
            json={
                "results": [
                    {"name": "warning_email", "label": "Warning"},
                    {"name": "error_email", "label": "Error"},
                ]
            },
            match=[request_kwargs_matcher({"timeout": sentinel.default_timeout})],
        )
        self.assertEqual(
            self.client.get_template_collections(),
            [
                TemplateCollection(name="warning_email", label="Warning"),
                TemplateCollection(name="error_email", label="Error"),
            ],
        )
        responses.assert_call_count(url, 1)


class TestProcessErrors(TestCase):
    def setUp(self):
        self.log_handler = LogCapture("typist", propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    @responses.activate
    def test_no_error(self):
        responses.add(responses.GET, "http://example.com/")
        response = process_errors(requests.get)("http://example.com")
        self.assertTrue(response.ok)

    @responses.activate
    def test_not_found(self):
        responses.add(responses.GET, "http://example.com/", status=404)
        with self.assertRaisesRegex(
            TemplateNotFound, "Template not found at url: http://example.com/"
        ):
            process_errors(requests.get)("http://example.com")

    @responses.activate
    def test_forbidden(self):
        responses.add(responses.GET, "http://example.com/", status=403)
        with self.assertRaisesRegex(
            ValueError, "403 Client Error: Forbidden for url: http://example.com/"
        ):
            process_errors(requests.get)("http://example.com")

    @responses.activate
    def test_internal_server_error(self):
        responses.add(responses.GET, "http://example.com/", status=500)
        with self.assertRaisesRegex(
            RuntimeError, "500 Server Error: Internal Server Error for url: http://example.com/"
        ):
            process_errors(requests.get)("http://example.com")

    @responses.activate
    def test_connection_error(self):
        responses.add(
            responses.GET,
            "http://example.com/",
            body=requests.ConnectionError("Failed to connect"),
        )
        with self.assertRaisesRegex(RuntimeError, "^Failed to connect$"):
            process_errors(requests.get)("http://example.com")


class HTTPTokenAuthTest(TestCase):
    def test_call(self):
        auth = HTTPTokenAuth("Gazpacho!")
        request = PreparedRequest()
        request.prepare_headers(None)

        result = auth(request)

        self.assertEqual(result.headers["Authorization"], "Token Gazpacho!")
